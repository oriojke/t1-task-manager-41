package ru.t1.didyk.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.model.ICommand;
import ru.t1.didyk.taskmanager.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "commands";
    @NotNull
    public static final String DESCRIPTION = "Show command list";

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@Nullable final ICommand command : commands) {
            @NotNull final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }
}
