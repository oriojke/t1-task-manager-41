package ru.t1.didyk.taskmanager.api.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;

public interface ITaskEndpointClient {

    @NotNull TaskBindToProjectResponse bindToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull TaskChangeStatusByIdResponse changeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull TaskClearResponse clear(@NotNull TaskClearRequest request);

    @NotNull TaskCompleteByIdResponse completeById(@NotNull TaskCompleteByIdRequest request);

    @NotNull TaskCompleteByIndexResponse completeByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull TaskCreateResponse create(@NotNull TaskCreateRequest request);

    @NotNull TaskListResponse listTasks(@NotNull TaskListRequest request);

    @NotNull TaskRemoveByIdResponse removeById(@NotNull TaskRemoveByIdRequest request);

    @NotNull TaskRemoveByIndexResponse removeByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull TaskShowByIdResponse showById(@NotNull TaskShowByIdRequest request);

    @NotNull TaskShowByIndexResponse showByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull TaskShowByProjectIdResponse showByProjectId(@NotNull TaskShowByProjectIdRequest request);

    @NotNull TaskStartByIdResponse startById(@NotNull TaskStartByIdRequest request);

    @NotNull TaskStartByIndexResponse startByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull TaskUnbindFromProjectResponse unbindFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull TaskUpdateByIdResponse updateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull TaskUpdateByIndexResponse updateByIndex(@NotNull TaskUpdateByIndexRequest request);

}
