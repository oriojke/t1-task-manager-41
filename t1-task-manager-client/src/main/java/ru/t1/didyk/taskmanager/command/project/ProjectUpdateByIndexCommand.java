package ru.t1.didyk.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.ProjectUpdateByIndexRequest;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-index";
    @NotNull
    public static final String DESCRIPTION = "Update project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX");
        System.out.println("ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setDescription(description);
        request.setName(name);
        serviceLocator.getProjectEndpointClient().updateByIndex(request);
    }

}
