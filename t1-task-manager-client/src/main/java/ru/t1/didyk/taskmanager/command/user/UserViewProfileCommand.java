package ru.t1.didyk.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.UserViewProfileRequest;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "view-user-profile";
    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @NotNull final UserDTO user = serviceLocator.getAuthEndpointClient().profile(request).getUser();
        System.out.println("[VIEW USER PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
