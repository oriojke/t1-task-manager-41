package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.request.TaskShowByProjectIdRequest;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-project-id";
    @NotNull
    public static final String DESCRIPTION = "Show tasks by project id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskEndpointClient().showByProjectId(request).getTasks();
        renderTasks(tasks);
    }

}
