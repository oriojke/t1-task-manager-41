package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.SessionDTO;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;

public interface IAuthService {

    @Nullable
    UserDTO registry(@NotNull String login, @NotNull String password, @NotNull String email);

    UserDTO check(@Nullable final String login, @Nullable final String password);

    SessionDTO validateToken(@Nullable final String token);

    void invalidate(@Nullable final SessionDTO session);

    @NotNull String login(@Nullable final String login, @Nullable final String password);

}
