package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);


    void clear(@Nullable String userId);


    @Nullable List<TaskDTO> findAll(@Nullable String userId);


    @Nullable List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator<TaskDTO> comparator);


    @Nullable TaskDTO add(@Nullable String userId, @NotNull TaskDTO model);


    @Nullable TaskDTO add(@NotNull TaskDTO model);


    boolean existsById(@Nullable String userId, @Nullable String id);


    boolean existsById(@Nullable String id);


    @Nullable TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @Nullable TaskDTO findOneById(@Nullable String id);


    @Nullable TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index);


    int getSize(@Nullable String userId);


    @Nullable TaskDTO remove(@Nullable String userId, @Nullable TaskDTO model);


    @Nullable TaskDTO remove(@Nullable TaskDTO model);


    @Nullable TaskDTO removeById(@Nullable String userId, @Nullable String id);


    @Nullable TaskDTO removeByIndex(@Nullable String userId, @Nullable Integer index);


    void removeAll(@Nullable String userId);


    @NotNull Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models);


    @NotNull Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models);


    @NotNull void update(final @NotNull TaskDTO object);


    @Nullable List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);


    @Nullable List<TaskDTO> findAll();
}
