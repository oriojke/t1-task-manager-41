package ru.t1.didyk.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.ISessionRepository;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.ISessionService;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.didyk.taskmanager.dto.model.SessionDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public @NotNull void update(@NotNull SessionDTO object) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.update(object);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) {
        @NotNull final List<SessionDTO> models = findAll(userId);
        models.forEach(model -> remove(model));
    }

    @Override
    public @Nullable List<SessionDTO> findAll(@Nullable String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable List<SessionDTO> result;
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            result = sessionRepository.findAllWithUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public @Nullable List<SessionDTO> findAll(@Nullable String userId, @Nullable Comparator<SessionDTO> comparator) {
        @NotNull final List<SessionDTO> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public @Nullable SessionDTO add(@Nullable String userId, @NotNull SessionDTO model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public @Nullable SessionDTO add(@NotNull SessionDTO model) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findOneById(id) != null;
    }

    @Override
    public @Nullable SessionDTO findOneById(@Nullable String userId, @Nullable String id) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable SessionDTO result;
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            result = sessionRepository.findOneByIdWithUserId(userId, id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public @Nullable SessionDTO findOneById(@Nullable String id) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable SessionDTO result;
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            result = sessionRepository.findOneById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public @Nullable SessionDTO findOneByIndex(@Nullable String userId, @Nullable Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public int getSize(@Nullable String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        int result = -1;
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            result = sessionRepository.getSize(userId);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @Override
    public @Nullable SessionDTO remove(@Nullable String userId, @Nullable SessionDTO model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public @Nullable SessionDTO remove(@Nullable SessionDTO model) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public @Nullable SessionDTO removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || id == null) return null;
        @Nullable final SessionDTO model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public @Nullable SessionDTO removeByIndex(@Nullable String userId, @Nullable Integer index) {
        @Nullable final SessionDTO model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void removeAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        findAll(userId).forEach(model -> remove(model));
    }

    @Override
    public @NotNull Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models) {
        models.forEach(model -> add(model));
        return models;
    }

    @Override
    public @NotNull Collection<SessionDTO> set(@NotNull Collection<SessionDTO> models) {
        findAll().forEach(model -> remove(model));
        return add(models);
    }

    @Override
    public @Nullable List<SessionDTO> findAll(@Nullable String userId, @Nullable Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    public @Nullable List<SessionDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable List<SessionDTO> result;
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            result = sessionRepository.findAll();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }
}
