package ru.t1.didyk.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.endpoint.*;
import ru.t1.didyk.taskmanager.api.service.*;
import ru.t1.didyk.taskmanager.endpoint.*;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;
import ru.t1.didyk.taskmanager.dto.model.UserDTO;
import ru.t1.didyk.taskmanager.service.*;
import ru.t1.didyk.taskmanager.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.didyk.taskmanager.command";

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = String.valueOf(propertyService.getServerPort());
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void initDemoData() {
        @NotNull final UserDTO test = userService.create("test", "test", "test@test.ru");
        @NotNull final UserDTO user = userService.create("user", "user", "user@test.ru");
        @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new ProjectDTO("TEST PROJECT 1", Status.IN_PROGRESS));
        projectService.add(test.getId(), new ProjectDTO("TEST PROJECT 2", Status.NOT_STARTED));
        projectService.add(test.getId(), new ProjectDTO("TEST PROJECT 3", Status.COMPLETED));
        projectService.add(test.getId(), new ProjectDTO("TEST PROJECT 4", Status.IN_PROGRESS));

        taskService.create(test.getId(), "TEST TASK 1");
        taskService.create(test.getId(), "TEST TASK 2");
    }

    public void start() {
        initPID();
        //initDemoData();
        loggerService.info("** WELCOME THE TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
