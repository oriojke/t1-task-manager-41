package ru.t1.didyk.taskmanager.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "43523";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAUL = "543654652";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "7893";

    @NotNull
    public static final String EMPTY_VALUE = "----";

    @NotNull
    public static final String GIT_BRANCH = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    public static final String GIT_COMMITER_NAME = "gitCommiterName";

    @NotNull
    public static final String GIT_COMMITER_EMAIL = "gitCommiterEmail";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_KEY_DEFAULT = "543645375634";

    @NotNull
    public static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    public static final String SESSION_TIMEOUT_DEFAULT = "10";

    @NotNull
    public static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    public static final String DATABASE_USERNAME_DEFAULT = "postgres";

    @NotNull
    public static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    public static final String DATABASE_PASSWORD_DEFAULT = "postgres";

    @NotNull
    public static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    public static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    public static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    public static final String DATABASE_HBM2DDL_KEY = "database.hbm2ddl";

    @NotNull
    public static final String DATABASE_HBM2DDL_DEFAULT = "update";

    @NotNull
    public static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    public static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    public static final String DATABASE_URL_DEFAULT = "jdbc:postgresql://localhost:5432/tm";

    @NotNull
    public static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    public static final String DATABASE_DRIVER_DEFAULT = "org.postgresql.Driver";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    @Override
    public String getCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getCommitterName() {
        return read(GIT_COMMITER_NAME);
    }

    @NotNull
    @Override
    public String getCommitterEmail() {
        return read(GIT_COMMITER_EMAIL);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_");
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    private void loadInternalConfig(@NotNull final Properties properties) throws IOException {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    private void loadExternalConfig(@NotNull final Properties properties) throws IOException {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public int getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
        return Integer.parseInt(value);
    }

    @Override
    public @NotNull String getDatabaseUser() {
        return getStringValue(DATABASE_USERNAME_KEY, DATABASE_USERNAME_DEFAULT);
    }

    @Override
    public @NotNull String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_KEY, DATABASE_PASSWORD_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY, DATABASE_DRIVER_DEFAULT);
    }

    @Override
    public @NotNull String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT_KEY, DATABASE_DIALECT_DEFAULT);
    }

    @Override
    public @NotNull String getHBM2DDL() {
        return getStringValue(DATABASE_HBM2DDL_KEY, DATABASE_HBM2DDL_DEFAULT);
    }

    @Override
    public @NotNull String getShowSql() {
        return getStringValue(DATABASE_SHOW_SQL_KEY, DATABASE_SHOW_SQL_DEFAULT);
    }
}
