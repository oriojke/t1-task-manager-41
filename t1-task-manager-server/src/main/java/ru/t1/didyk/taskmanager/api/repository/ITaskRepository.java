package ru.t1.didyk.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @NotNull
    @Select("select * from tasks where user_id=#{userId} and project_id=#{projectId}")
    List<TaskDTO> findAllByProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId);


    @Select("select * from tasks where user_id=#{userId}")
    @Results(value = {
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<TaskDTO> findAllWithUserId(@Nullable @Param("userId") String userId);


    @Select("select * from tasks")
    @Results(value = {
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<TaskDTO> findAll();

    @Insert("insert into tasks (id, name, description, user_id, created, status, project_id)" +
            "values (#{id}, #{name}, #{description}, #{userId}, #{created}, #{status}, #{projectId})")
    void add(@NotNull TaskDTO model);

    @Select("select * from tasks where id=#{id} and user_id=#{userId} limit 1")
    @Nullable TaskDTO findOneByIdWithUserId(@Nullable @Param("userId") String userId, @Nullable @Param("id") String id);

    @Select("select * from tasks where id=#{id} limit 1")
    @Nullable TaskDTO findOneById(@Nullable @Param("id") String id);

    @Select("select count(*) from tables where user_id=#{userId}")
    int getSize(@Nullable @Param("userId") String userId);

    @Delete("delete from tasks where id=#{id}")
    void remove(@Nullable TaskDTO model);

    @Update("update tasks set name = #{name}, description = #{description}, user_id = #{user_id}, created = #{created}, " +
            "status = #{status}, project_id = #{project_id} where id=#{id}")
    void update(final @NotNull TaskDTO object);
}
