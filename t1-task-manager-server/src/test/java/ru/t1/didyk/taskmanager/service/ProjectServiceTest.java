package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.marker.UnitCategory;

import static ru.t1.didyk.taskmanager.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final ProjectService projectService = new ProjectService(connectionService);

    @Before
    public void before() {
        projectService.add(USER1.getId(), USER_PROJECT);
        projectService.add(USER2.getId(), ADMIN_PROJECT);
    }

    @After
    public void after() {
        projectService.clear(USER1.getId());
        projectService.clear(USER2.getId());
    }

    @Test
    public void add() {
        Assert.assertNotNull(projectService.add(USER1.getId(), USER_PROJECT2));
        Assert.assertNotNull(projectService.add(USER_PROJECT3));
        Assert.assertFalse(projectService.add(USER_PROJECT_LIST).isEmpty());
    }

    @Test
    public void changeProjectStatus() {
        Assert.assertNotNull(projectService.changeProjectStatusById(USER1.getId(), USER_PROJECT.getId(), Status.NOT_STARTED));
        Assert.assertNotNull(projectService.changeProjectStatusByIndex(USER1.getId(), 0, Status.IN_PROGRESS));
    }

    @Test
    public void create() {
        Assert.assertNotNull(projectService.create(USER1.getId(), "NEW NAME"));
        Assert.assertNotNull(projectService.create(USER1.getId(), "NEW NAME 2", "NEW DESCRIPTION"));
    }

    @Test
    public void clear() {
        projectService.clear(USER1.getId());
        Assert.assertTrue(projectService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void find() {
        Assert.assertFalse(projectService.findAll().isEmpty());
        Assert.assertFalse(projectService.findAll(USER1.getId()).isEmpty());
        Assert.assertFalse(projectService.findAll(USER1.getId(), Sort.BY_NAME.getComparator()).isEmpty());
        Assert.assertFalse(projectService.findAll(USER1.getId(), Sort.BY_STATUS.getComparator()).isEmpty());
        Assert.assertFalse(projectService.findAll(USER1.getId(), Sort.BY_CREATED.getComparator()).isEmpty());
        Assert.assertFalse(projectService.findAll(USER1.getId(), Sort.BY_NAME).isEmpty());
        Assert.assertFalse(projectService.findAll(USER1.getId(), Sort.BY_STATUS).isEmpty());
        Assert.assertFalse(projectService.findAll(USER1.getId(), Sort.BY_CREATED).isEmpty());
        Assert.assertNotNull(projectService.findOneById(USER1.getId(), USER_PROJECT.getId()));
        Assert.assertNotNull(projectService.findOneByIndex(USER1.getId(), 0));
    }

    @Test
    public void remove() {
        Assert.assertNotNull(projectService.remove(USER1.getId(), USER_PROJECT));
        Assert.assertNotNull(projectService.remove(ADMIN_PROJECT));
    }

    @Test
    public void removeAll() {
        projectService.removeAll(USER1.getId());
        Assert.assertTrue(projectService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(projectService.removeById(USER1.getId(), USER_PROJECT.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(projectService.removeByIndex(USER1.getId(), 0));
    }

    @Test
    public void updateById() {
        Assert.assertNotNull(projectService.updateById(USER1.getId(), USER_PROJECT.getId(), "NEW NAME", "NEW DESCRIPTION"));
    }

    @Test
    public void updateByIndex() {
        Assert.assertNotNull(projectService.updateByIndex(USER1.getId(), 0, "NEW NAME", "NEW DESCRIPTION"));
    }
}
