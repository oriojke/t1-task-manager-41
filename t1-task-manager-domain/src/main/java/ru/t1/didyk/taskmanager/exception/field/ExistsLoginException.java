package ru.t1.didyk.taskmanager.exception.field;

public final class ExistsLoginException extends AbstractFieldException {

    public ExistsLoginException() {
        super("Error! Login exists.");
    }

}
