package ru.t1.didyk.taskmanager.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.enumerated.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "users")
public class UserDTO extends AbstractModelDTO {

    private final static long serialVersionUID = 1;

    @Column
    @Nullable
    private String login;

    @Nullable
    @Column(name = "password")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Column
    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @Nullable
    @Column(name = "is_locked")
    private Boolean locked = false;

}
