package ru.t1.didyk.taskmanager.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskBindToProjectResponse extends AbstractTaskResponse {

    public TaskBindToProjectResponse(@Nullable TaskDTO task) {
        super(task);
    }
}
