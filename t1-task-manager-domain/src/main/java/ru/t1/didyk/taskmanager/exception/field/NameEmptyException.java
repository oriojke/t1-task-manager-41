package ru.t1.didyk.taskmanager.exception.field;

public final class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("Error! Name is empty.");
    }

}
